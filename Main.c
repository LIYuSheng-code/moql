#include <msp430.h>
#include <string.h>
#include <fonction.h>
#include <stdlib.h>
#include <stdio.h>

#define CMDLEN  20                      // Nombre de caracteres pouvantre saisis

#define TRUE    1                       // TRUE vaut 1
#define FALSE   0                       // FALSE vaut 0

#define LF      0x0A                    // line feed or \n
#define CR      0x0D                    // carriage return or \r
#define BSPC    0x08                    // back space
#define DEL     0x7F                    // SUPRESS
#define ESC     0x1B                    // escape

int state = 0;

/*
 * Variables globals
 */
// static const char spi_in = 0x37;
unsigned char tab[CMDLEN];      // tableau de caracteres lie a la commande user
//unsigned char car = 0x30;       // 0
unsigned int  nb_cara = 0;
unsigned char inttab = FALSE;   // call interpreteur()


/*
 * Envoie d'une caractere sur USCI en SPI 3 fils Master Mode
 */
void Send_char_SPI(unsigned char c)
{
    while ((UCB0STAT & UCBUSY));            // attend que USCI_SPI soit dispo.
    while(!(IFG2 & UCB0TXIFG));             // attente de fin du dernier envoi (UCB0TXIFG ?1 quand UCB0TXBUF vide)
    UCB0TXBUF = c;                          // Mettre le caractere dans le tampon de transmission
}

/*
 * test
 */

void main(void)
{
    // Stop watchdog timer to prevent time out reset
    WDTCTL = WDTPW | WDTHOLD;

    //Variables for operation_command
    unsigned char c;
    char  tab[20];      // tableau de caractere lie a la commande user
    unsigned int nb_cara1=0;    // compteur nombre carateres saisis

    InitUART();

    init_temp_moteur();

    init_USCI();

    erro_avoid();

    changeligne();

    while(1){

        if( nb_cara1<(19) )
                {
                    RXdata(&c);
                       if( (tab[nb_cara1]=c) != CR )
                       {
                           TXdata(c);
                           nb_cara1++;
                       }
                       else{
                        tab[nb_cara1]=0x00;
                        if(strcmp(tab, "h") == 0){
                            changeligne();
                            putsUART(" Menu help         ");  //19 char
                            putsUART(" q---demi gauche   ");
                            putsUART(" w---avancer       ");
                            putsUART(" e---demi droit    ");
                            putsUART(" a---tourner gauche");
                            putsUART(" s---reculer       ");
                            putsUART(" d---tourner droit ");
                            putsUART(" autre---arreter   ");
                            TXdata('>');
                        }else if(strcmp(tab, "w") == 0){
                            changeligne();
                            state = 1;
                            putsUART("avancer stature    ");
                            TXdata('>');
                            Operation(state);
                        }else if(strcmp(tab, "a") == 0){
                            changeligne();
                            state = 2;
                            putsUART("tourne_G  stature   ");
                            TXdata('>');
                            Operation(state);
                        }else if(strcmp(tab, "d") == 0){
                            changeligne();
                            state = 3;
                            putsUART("tourne_D  stature   ");
                            TXdata('>');
                            Operation(state);
                        }else if(strcmp(tab, "s") == 0){
                            changeligne();
                            state = 4;
                            putsUART("reculer stature     ");
                            TXdata('>');
                            Operation(state);
                        }else if(strcmp(tab, "q") == 0){
                            changeligne();
                            state = 5;
                            putsUART("reculer stature     ");
                            TXdata('>');
                            Operation(state);
                        }else if(strcmp(tab, "e") == 0){
                            changeligne();
                            state = 6;
                            putsUART("reculer stature     ");
                            TXdata('>');
                            Operation(state);
                        }else if(strcmp(tab, "m") == 0){
                            changeligne();
                            if(inttab){
                                while((UCB0STAT & UCBUSY));
                                Send_char_SPI(0x30);
                                inttab = FALSE;
                            }else{
                                __bis_SR_register(LPM4_bits | GIE);
                            }
                            putsUART("moteur stature      ");
                            TXdata('>');
                        }else{
                            changeligne();
                            state = 0;
                            putsUART("arreter stature       ");
                            putsUART("\rEntrez 'h'for help  ");
                            TXdata('>');
                            Operation(state);
                        }
                         nb_cara1=0;
                       }
                }

    }
}

// R O U T I N E S   D ' I N T E R R U P T I O N S
/*
 * VECTEUR INTERRUPTION USCI RX
 */

#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCIAB0RX_ISR()
{
    //UART
    if (IFG2 & UCA0RXIFG)
    {
        while(!(IFG2 & UCA0RXIFG));
        tab[nb_cara]=UCA0RXBUF;         // lecture la caractere

        while(!(IFG2 & UCA0TXIFG));    // attente de fin du dernier envoi (UCA0TXIFG ?1 quand UCA0TXBUF vide) / echo
        UCA0TXBUF = tab[nb_cara];

        if( tab[nb_cara] == ESC)        //Fonction ESC
        {
            nb_cara = 0;
            tab[1] = 0x00;
            tab[0] = CR;
        }

        if( (tab[nb_cara] == CR) || (tab[nb_cara] == LF)) //Appuyez sur Entre pour inttab vers True
        {
            tab[nb_cara] = 0x00;
            inttab = TRUE;
            nb_cara = 0;
            __bic_SR_register_on_exit(LPM4_bits);   // OP mode !
        }
        else if( (nb_cara < CMDLEN) && !((tab[nb_cara] == BSPC) || (tab[nb_cara] == DEL)) ) //Fonction DEL
        {
            nb_cara++;
        }
        else
        {
            tab[nb_cara] = 0x00;
            nb_cara--;
        }
    }

    //SPI
    else if (IFG2 & UCB0RXIFG)
    {
        while( (UCB0STAT & UCBUSY) && !(UCB0STAT & UCOE) );
        while(!(IFG2 & UCB0RXIFG));
        tab[0] = UCB0RXBUF;
        tab[1] = 0x00;
    }
}
//End ISR