/*
 * -----------------------------------------------------
 *  P R O G R A M M A T I O N   S P I   E S I G E L E C
 *
 * Lauchpad v1.0 support
 * M S P 4 3 0 G 2 2 3 1   -   SPI/SLAVE 3 Wires
 * La carte Launchpad est raccord��e en SPI via l'USI B0
 *      SCLK : P1.5 / SCLK
 *      SIMO : P1.7 / SDI
 *      MOSI : P1.6 / SDO
 *
 * LIU Baikun et LI Yusheng
 * Groupe: ISE-OC
 * --------------------------------------------------------------
 *
 * A la reception du caract��re 1 la LED Rouge P1.0 est allum��e
 * A la reception du caract��re 0 la LED Rouge P1.0 est eteinte
 */

#include <msp430.h> 
#include <intrinsics.h>

/*
 * Prototypes
 */
void initBOARD();
void init_USI();

/*
 * Definitions
 */
#define ZERO   0x00     // Exprime 0x00
#define LED_R  BIT0     // Red LED
#define LED_G  BIT6     // Green LED
#define MOTORS_FREQUENCY 33333
#define MOTORS_DUTYCYCLE 1250
#define STEP_ANGLE 72
#define PW_MIN 504
#define PW_MAX 2448

/*
 * Variables globales
 */
volatile unsigned char RXData;
unsigned int up = 0; // sens de variation
unsigned int cmd = 0; // periode du signal de commande moteur

/* ----------------------------------------------------------------------------
 * Fonction d'initialisation de la carte 2231
 * Entrees: -
 * Sorties: -
 */
void initBOARD()
{
    WDTCTL = WDTPW | WDTHOLD;    // Stop watchdog timer to prevent time out reset
    if((CALBC1_1MHZ==0xFF) || (CALDCO_1MHZ==0xFF))
    {
        __bis_SR_register(LPM4_bits);
    }else{
        DCOCTL = 0;             // Factory Set
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = (0 | CALDCO_1MHZ);
    }
    //Secure mode
    P1SEL = 0x00; // GPIO
    P1DIR = 0x00; // IN
    P1SEL &= ~(BIT0 | BIT6); // Port 1, ligne 0 et 6 en fonction primaire
    P1DIR |= (BIT0 | BIT6 ); // P1.0 et P1.6 en sortie
    P1OUT &= ~(BIT0 | BIT6); // P1.0 et P1.6 �� 0

    P1DIR &=~BIT3; // Port 1 ligne 3 en entr��e
    P1REN |= BIT3; // Activation de la resistance de tirage
    P1OUT |= BIT3; // Resistance en Pull-Up
    P1IES &=~BIT3; // Detection de front montant
    P1IE |= BIT3; // Activation des interruptions sur P1.3

    P1SEL |= BIT2; // Port 2, ligne 2 en fonction secondaire
    P1DIR |= BIT2; // Port 2, ligne 2 en sortie
}

/* ----------------------------------------------------------------------------
 * Fonction d'initialisation de l'USI POUR SPI SUR USI
 * Entree : -
 * Sorties: -
 */
void init_USI()
{
    USICTL0 |= USISWRST;                    // USI Config. for SPI 3 wires Slave Op.
    USICTL1 = 0;                            // P1SEL Ref. p41,42 SLAS694J used by USIPEx
    // 3 wire, mode Clk&Ph
    USICTL0 |= (USIPE7 | USIPE6 | USIPE5);  //SDI-SDO-SCLK for these Ports Enable
    USICTL0 |= (USILSB);                    //LSB first
    USICTL0 |= (USIOE);                     //Output Enable
    USICTL0 |= (USIGE);                     //General Output Enable Latch

    // Slave Mode SLAU144J
    USICTL0 &= ~(USIMST);                   //Select slave
    USICTL1 |= USIIE;                       //Counter Interrupt enable
    USICTL1 &= ~(USICKPH | USII2C);         //Mode: Clock and It is I2C Mode

    //Clock Control Register
    USICKCTL = 0;                           // No Clk Src in slave mode
    USICKCTL &= ~(USICKPL | USISWCLK);      // Polarity - Input ClkLow

    //Bit Counter Register
    USICNT = 0;
    USICNT &= ~(USI16B | USIIFGCC );        // Only lower 8 bits used 14.2.3.3 p 401 slau144j
    USISRL = 0x23;                          // Hash, just mean ready; USISRL Vs USIR by ~USI16B set to 0
    USICNT = 0x08;

    // Wait for the SPI clock to be idle (low).
    while ((P1IN & BIT5)) ;
    USICTL0 &= ~USISWRST;
    __bis_SR_register(LPM4_bits | GIE);     // General interrupts enable & Low Power Mode
}

/* ----------------------------------------------------------------------------
* FONCTION D'INITIALISATION DU TIMER
* Entree : -
* Sorties: -
*/
void init_Timer( void )
{
    TA0CTL &= ~MC_0; // arret du timer
    TA0CCR0 = MOTORS_FREQUENCY; // periode du signal PWM 2KHz
    TA0CTL = (TASSEL_2 | MC_1 | ID_0);
    TA0CCTL1 = 0 | OUTMOD_7; // select timer compare mode
}

/*
 * main.c
 */
void main()
{
    initBOARD();
    init_USI();
    init_Timer();
    cmd = MOTORS_DUTYCYCLE;
    up = 1;
    TA0CCR1 = cmd;
    __bis_SR_register(LPM0_bits | GIE);
}

// R O U T I N E S   D ' I N T E R R U P T I O N S
/*
 * VECTEUR INTERRUPTION USI
 */
#pragma vector=USI_VECTOR
__interrupt void universal_serial_interface()
{
    unsigned int i = 0;

    while( !(USICTL1 & USIIFG) );   // Waiting char by USI counter flag
    RXData = USISRL;

    if(RXData == 0x30) //If the input buffer is 0x31 (mainly to read the buffer)
    {
        P1OUT &= ~BIT0;                 //Turn off two LED
        P1OUT &= ~BIT6;
    }else if(RXData == 0x31)
    {
        P1OUT |= BIT0;                 //Turn on two LED
        P1OUT |= BIT6;
    }else if(RXData == 0x32){
        for(i=0;i<50;i++){
            P1OUT ^= BIT0;
            P1OUT ^= BIT6;
            __delay_cycles(100000);     //Red LED flashes for five seconds

        }
    }else if(RXData == 0x33)
    {
        P1OUT &= ~BIT0;                 //Turn off Red LED
    }else if(RXData == 0x34)
    {
        P1OUT |= BIT0;                  //Turn on Red LED
    }else if(RXData == 0x35)
    {
        P1OUT &= ~BIT6;                 //Turn off Green LED
    }else if(RXData == 0x36)
    {
        P1OUT |= BIT6;                  //Turn on Green LED
    }else if(RXData == 0x37){
        if( !up ) // Sens d��croissant
        {
            if (cmd > (PW_MIN+STEP_ANGLE) ) // Si P��riode mini non encore atteinte
            {
                cmd -= STEP_ANGLE; // D��cr��menter la p��riode
            }
            else // Sinon
            {
                cmd = PW_MIN; // Ajuster la p��riode
                up = 1; // Changer le sens de boucle
            }
            P1OUT ^= BIT0; // Faire clignoter la Led a chaque it��ration
        }else{
            P1OUT &=~BIT0; // Eteindre la Led de P1.0
            if(cmd < (PW_MAX-STEP_ANGLE) ) // Si P��riode inf��rieure au max
            {
                cmd += STEP_ANGLE; // Augmenter la p��riode
            }
            else // Sinon
            {
                    cmd = PW_MAX; // Ajuster la p��riode
                    up = 0; // Inverser le sens de boucle
            }
            P1OUT ^= BIT6; // Faire clignoter la Led
        }
        TA0CCR1 = cmd; // Modifier la valeur de commptage Timer
    }
    USISRL = RXData;
    USICNT &= ~USI16B;  // Re-load counter & ignore USISRH
    USICNT = 0x08;      // 8 bits count, that re-enable USI for next transfert
}
//End ISR
