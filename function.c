#include <msp430.h>
#include <string.h>
#include <fonction.h>

#define CR      0x0D
#define BS      0x08
#define CR      0x0D
#define BS      0x08
#define RELEASE "\r\t\tSPI ver1.3"      // Exprime la version
#define PROMPT  "\r\n>"                 // Indique le d�but de chaque commande
#define CMDLEN  20                      // Nombre de caracteres pouvant �tre saisis

#define TRUE    1                       // TRUE vaut 1
#define FALSE   0                       // FALSE vaut 0

#define ZERO    0x00                    // Exprime 0x00
#define LF      0x0A                    // line feed or \n
#define CR      0x0D                    // carriage return or \r
#define BSPC    0x08                    // back space
#define DEL     0x7F                    // SUPRESS
#define ESC     0x1B                    // escape

#define _CS         BIT4                // chip select for SPI Master->Slave ONLY on 4 wires Mode
#define SCK         BIT5                // Serial Clock
#define DATA_OUT    BIT6                // DATA out
#define DATA_IN     BIT7                // DATA in

#define LED_R       BIT0                // Red LED on Launchpad


unsigned int etat;

/*-----------------------------------------------------UART-------------------------------------------------------------------*/
/*
 * InitUART
 */
void InitUART(void)
{
    P1SEL |= (BIT1 | BIT2);                     // P1.1 = RXD, P1.2=TXD
    P1SEL2 |= (BIT1 | BIT2);                    // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 = UCSWRST;                         // SOFTWARE RESET
    UCA0CTL1 |= UCSSEL_3;                       // SMCLK (2 - 3)

    UCA0CTL0 &= ~(UCPEN | UCMSB | UCDORM);
    UCA0CTL0 &= ~(UC7BIT | UCSPB | UCMODE_3 | UCSYNC); // dta:8 stop:1 usci_mode3uartmode
    UCA0CTL1 &= ~UCSWRST;                   // **Initialize USCI state machine**

    UCA0BR0 = 104;                              // 1MHz, OSC16, 9600 (8Mhz : 52) : 8/115k
    UCA0BR1 = 0;                                // 1MHz, OSC16, 9600
    UCA0MCTL = 10;

    /* Enable USCI_A0 RX interrupt */
    IE2 |= UCA0RXIE;
}

/*
 * UART Read data
 */
void RXdata(unsigned char *c)
{
    while (!(IFG2&UCA0RXIFG));              // buffer Rx USCI_A0 plein ?
    *c = UCA0RXBUF;
}

/*
 * PutsChar
 */
void putsUART(unsigned char buffer[30]){
    unsigned int i;
    for(i=0;i<19;i++){
        TXdata( buffer[i]);
    }
    TXdata('\n');
    TXdata('\r');
}

/*
 * Text data
 */
void TXdata( unsigned char c )
{
    while (!(IFG2 & UCA0TXIFG));  // USCI_A0 TX buffer ready?
    UCA0TXBUF = c;              // TX -> RXed character
}

/*
 * change line function
 */
void changeligne(void){
    TXdata('\n');
    TXdata('\r');
    TXdata('>');
}

/*------------------------------------------------------SPI-------------------------------------------------------------------*/
/*
 * initialisation of SPI
 */
void init_USCI()
{
    __delay_cycles(250);                    // Waiting Slave SYNC
    UCB0CTL1 = UCSWRST;                     // SOFTWARE RESET
    IFG2 &= ~(UCB0TXIFG | UCB0RXIFG);       // clearing IFg /16.4.9/p447/SLAU144j in order to set by setting UCSWRST just before

    UCB0CTL0 &= ~(UCCKPH | UCCKPL);         //SPI Mode 1 and Clock inactive state is low.
    UCB0CTL0 &= ~(UCMSB);                   //LSB premier
    UCB0CTL0 &= ~(UC7BIT);                  //En 8 bits

    UCB0CTL0 |= (UCMST);                    //CLK by USCI
    UCB0CTL0 |= (UCMODE_0);                 //SPI 3 fils MASTER Mode
    UCB0CTL0 |= (UCSYNC);                   //Mode synchrone (SPI)
    UCB0CTL1 |= UCSSEL_2;

    UCB0BR0 = 0x0A;                         //1MHz, OSC16, 9600 (8Mhz : 52) : 8/115k
    UCB0BR1 = 0x00;                         //1MHz, OSC16, 9600

    P1SEL  |= ( SCK | DATA_OUT | DATA_IN);  //CLK-1.5, MISO-1.6 et MOSI-1.7
    P1SEL2 |= ( SCK | DATA_OUT | DATA_IN);  // SPI : Fonctions secondaires
    UCB0CTL1 &= ~UCSWRST;                   // activation USCI
}

/*
 * low power mode if in error
 */
void erro_avoid(){
    if(CALBC1_1MHZ==0xFF || CALDCO_1MHZ==0xFF)
        {
            __bis_SR_register(LPM4_bits); // Low Power Mode #trap on Error
        }
        else
        {
            // Factory parameters
            BCSCTL1 = CALBC1_1MHZ;
            DCOCTL = CALDCO_1MHZ;
        }
}



/*
 * Emission d'une chain de caracteres
 */
void envoi_msg_UART(unsigned char *msg)
{
    unsigned int i = 0;
    for(i=0 ; msg[i] != 0x00 ; i++)
    {
        while(!(IFG2 & UCA0TXIFG));    //attente de fin du dernier envoi (UCA0TXIFG ?1 quand UCA0TXBUF vide)
        UCA0TXBUF=msg[i];
    }
}


/*------------------------------------------------------Moteur and robot-------------------------------------------------------------------*/
/*
 * Initialiser Temp , PWM et Moteur
 */

void init_temp_moteur()
{
    //Init le temp total
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    TACCR0 = 500;
    TA0CTL |= MC_3;
    TA0CTL |= TASSEL_2;
    TA0CTL |= ID_3;

    //INIT PORT2
    P2DIR &= ~(BIT3 | BIT0);
    P2DIR |= BIT1 | BIT2 | BIT4 | BIT5;
    P2SEL |= BIT2 | BIT4;
    P2SEL2 &= ~(BIT2 | BIT4);

    //INIT TIMER
    TA1CTL = 0 | (TASSEL_2 | ID_0);
    TA1CTL |= MC_1;
    TA1CCTL1 |= OUTMOD_7;
    TA1CCTL2 |= OUTMOD_7;
    TA1CCR0 = 400; //r��f��rence du signal
    TA1CCR1 = 0;  //Roue A
    TA1CCR2 = 0; //Roue B
    P2OUT &= ~(BIT0 | BIT3);
}

/*
 * avancer vitesse 100
 */
void avancer()
{
    //SENS ET MOTEUR
    P2OUT &= ~(BIT0 | BIT3 | BIT1);
    P2OUT |= (BIT2 | BIT4 | BIT5);
    TA1CCR1 = 300;  //Vitesse de la roue A
    TA1CCR2 = 300; //Vitesse de la roue B
}

/*
 * reculer vitesse 100
 */
void reculer()
{
    //SENS ET MOTEUR
    P2OUT &= ~(BIT0 | BIT3 | BIT5);
    P2OUT |= (BIT2 | BIT4 | BIT1);
    TA1CCR1 = 300;  //Vitesse de la roue A
    TA1CCR2 = 300; //Vitesse de la roue B
}

/*
 * Gauche vitesse 100
 */
void tourne_G()
{
    //SENS ET MOTEUR
    P2OUT &= ~(BIT0 | BIT3);
    P2OUT |= (BIT1 | BIT5 | BIT2 | BIT4);
    TA1CCR1 = 300;  //Vitesse de la roue A
    TA1CCR2 = 300; //Vitesse de la roue B
}

/*
 * Droit vitesse 100
 */
void tourne_D()
{
    //SENS ET MOTEUR
    P2OUT &= ~(BIT1 | BIT5 | BIT0 | BIT3);
    P2OUT |= (BIT2 | BIT4);
    TA1CCR1 = 300;  //Vitesse de la roue A
    TA1CCR2 = 300; //Vitesse de la roue B
}

/*
 * arret
 */
void arreter()
{
    //SENS ET MOTEUR
    TA1CCR1 = 0;  //Vitesse de la roue A
    TA1CCR2 = 0; //Vitesse de la roue B
}

/*
 * capture ultra_rouge
 */

int capture_ultra_rouge() {

    if((P1IN & BIT7)==BIT7) {
        return 1;
    }else{
        return 0;
    }
}

/*
 * capture ultra_son
 */

int capture_ultra_son(){
    return 0;
}

/*------------------------------------------------------operation et les autres-------------------------------------------------------------------*/

/*
 * InitLED
 */
void InitLED( void )
{
   P1DIR |= BIT0;  // port 1.0  en sortie
   P1OUT &= ~BIT0;  // force etat bas P1.0 - LED1
}

/*
 * Initial of the delay time
 */

void delay_timer(int time){
    int i;
    for(i=0;i<time;i++){
        while(!(TA0CTL & TAIFG));
        TA0CTL &= ~TAIFG;
    }
}


/*
 * operation of robot
 */
void Operation(int choix){
    if(choix == 1){
        avancer();
        etat=1;
    }else if(choix == 2){
        tourne_G();
        delay_timer(50);
        if(etat == 1){
          avancer();
        }
        if(etat == 2){
            reculer();
        }
    }else if(choix == 3){
        tourne_D();
        delay_timer(50);
        if(etat == 1){
            avancer();
        }
        if(etat == 2){
             reculer();
        }
    }else if(choix == 4){
        reculer();
        etat=2;
    }else if(choix == 5){
        tourne_G();
        delay_timer(25);
        if(etat == 1){
          avancer();
        }
        if(etat == 2){
            reculer();
        }
    }else if(choix == 6){
        tourne_G();
        delay_timer(25);
        if(etat == 1){
          avancer();
        }
        if(etat == 2){
            reculer();
        }
    }else{
        arreter();
    }
}