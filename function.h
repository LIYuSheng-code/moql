/*
 * InitUART
 */
void InitUART(void);

/*
 * InitLED
 */
void InitLED( void );

/*
 * Read data
 */
void RXdata(unsigned char *c);

/*
 * Text data
 */
void TXdata( unsigned char c );

/*
 * PutsChar
 */
void putsUART(unsigned char buffer[30]);

/*
 * change line function
 */
void changeligne(void);

/*
 * Initial of the delay time
 */

void delay_timer(int time);

/*
 * Initialiser Temp , PWM et Moteur
 */

void init_temp_moteur();

/*
 * avancer vitesse 200
 */
void avancer();

/*
 * reculer vitesse 200
 */
void reculer();

/*
 * Gauche vitesse 200
 */
void tourne_G();

/*
 * Droit vitesse 200
 */
void tourne_D();

/*
 * arret
 */
void arreter();


/*
 * capture ultra_rouge
 */

int capture_ultra_rouge();

/*
 * capture ultra_son
 */

int capture_ultra_son();

/*
 * operation of robot
 */
void Operation(int choix);

/*
 * initialisation of SPI
 */
void init_USCI();

/*
 * low power mode if in error
 */
void erro_avoid();